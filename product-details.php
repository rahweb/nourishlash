<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="img pro-det">
            <div class="overlay" style="height: 40.5%;">
            </div>
            <img style="width:100%" src="images/slider/Layer copy.png" alt="">
        </div>
        <div class="container">
            <div class="title-e text-left">
                <br>
                <ul>
                    <li class="home"><a href="#">HOME</a></li>
                    <li><a>PRODUCT DETAILS</a></li>
                </ul>
                <br>
            </div>
        </div>
        <div class="product-details">
            <div class="row">
                <?php include("blocks/research.php");?>
            </div>
            <div class="container">
                <div class="col-lg-12">
                    <h3 style="border-bottom: 2px solid #98356e;" class="text-center">NourishLash Customer Reviews</h3>
                    <div class="media comment-box">
                        <div class="media-left">
                            <a href="#">
                                <img class="img-responsive user-photo" src="images/user.png">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Gloria. M</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="Collapse">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <button style="width:100%" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                                    class="btn btn-primary">SUBMIT YOUR REVIEW
                                            </button>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div col-md-5 img-thumbnail>
                                            <div id="feedback">
                                                <div class="container">
                                                    <div class="form-area">
                                                        <form method="POST" role="form" accept-charset="UTF-8" id=""
                                                            action="{{URL::action('Site\ProductController@postComment')}}">
                                                            <br style="clear:both">
                                                            <input style="width: 100%" class="form-control" type="hidden"
                                                                name="product_id"
                                                                value="{{$product->id}}"/>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name"
                                                                    name="name" placeholder="Name" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" id="email"
                                                                    name="email" placeholder="Email" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea class="form-control" type="textarea" id="content"
                                                                        name='content' placeholder="Message"
                                                                        rows="7">
                                                                </textarea>
                                                            </div>
                                                            <br>
                                                            <button type="submit" class="btn btn-primary pull-right"
                                                                >Submit Form
                                                            </button>
                                                            <br>
                                                            <br>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <?php include("blocks/footer.php");?>
    </body>
    <?php include("blocks/script.php");?>
</html>