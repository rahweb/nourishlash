

$(document).ready(function() {
    $(".menu-icon").on("click", function() {
          $("nav ul").toggleClass("showing");
    });
});


$(function() {
var header = $("header");

      $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                  header.addClass("scrolled");
            } else {
                  header.removeClass("scrolled");
            }
      });

});

(function($) {
      $.fn.menumaker = function(options) {  
       var cssmenu = $(this), settings = $.extend({
         format: "dropdown",
         sticky: false
       }, options);
       return this.each(function() {
         $(this).find(".button").on('click', function(){
           $(this).toggleClass('menu-opened');
           var mainmenu = $(this).next('ul');
           if (mainmenu.hasClass('open')) { 
             mainmenu.slideToggle().removeClass('open');
           }
           else {
             mainmenu.slideToggle().addClass('open');
             if (settings.format === "dropdown") {
               mainmenu.find('ul').show();
             }
           }
         });
         cssmenu.find('li ul').parent().addClass('has-sub');
      multiTg = function() {
           cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
           cssmenu.find('.submenu-button').on('click', function() {
             $(this).toggleClass('submenu-opened');
             if ($(this).siblings('ul').hasClass('open')) {
               $(this).siblings('ul').removeClass('open').slideToggle();
             }
             else {
               $(this).siblings('ul').addClass('open').slideToggle();
             }
           });
         };
         if (settings.format === 'multitoggle') multiTg();
         else cssmenu.addClass('dropdown');
         if (settings.sticky === true) cssmenu.css('position', 'fixed');
      resizeFix = function() {
        var mediasize = 1000;
           if ($( window ).width() > mediasize) {
             cssmenu.find('ul').show();
           }
           if ($(window).width() <= mediasize) {
             cssmenu.find('ul').hide().removeClass('open');
           }
         };
         resizeFix();
         return $(window).on('resize', resizeFix);
       });
        };
      })(jQuery);
      
      (function($){
      $(document).ready(function(){
      $("#cssmenu").menumaker({
         format: "multitoggle"
      });
      });
      })(jQuery);

$('.carousel').carousel({
      interval: 3000
  });


  function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  
  function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
  }
  