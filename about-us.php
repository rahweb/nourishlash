<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="img pro-det">
            <div class="overlay" style="height: 40.5%;">
            </div>
            <img style="width:100%" src="images/slider/Layer copy.png" alt="">
        </div>
        <div class="container">
            <div class="title-e text-left">
                <br>
                <ul>
                    <li class="home"><a href="#">HOME</a></li>
                    <li><a>ABOUT US</a></li>
                </ul>
                <br>
            </div>
        </div>
        <div class="about-section paddingTB60 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-6">
                        <div class="about-img">
                            <img style="width:100%" src="images/product/Layer 4 copy.png" alt="">
                        </div>
                    </div>	
                    <div class="col-md-7 col-sm-6">
                        <div class="about-title clearfix">
                            <h1>ABOUT <span>EYELASHSERUM</span></h1>
                            <h3>Lorem ipsum dolor sit amet </h3>
                            <p class="about-paddingB">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquet dolor libero, eget venenatis mauris finibus dictum. Vestibulum quis elit eget neque porttitor congue non sit amet dolor. Proin pretium purus a lorem ornare </p>
                            <p>sed lobortis pulvinar. Integer laoreet mi id eros porta euismod. Suspendisse potenti. Nulla eros mauris, convallis et sem tempus, viverra hendrerit sapien</p>
                            <p>sed lobortis pulvinar. Integer laoreet mi id eros porta euismod. Suspendisse potenti. Nulla eros mauris, convallis et sem tempus, viverra hendrerit sapien</p>
                            <p>sed lobortis pulvinar. Integer laoreet mi id eros porta euismod. Suspendisse potenti. Nulla eros mauris, convallis et sem tempus, viverra hendrerit sapien</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
    </body>
    <?php include("blocks/script.php");?>
</html>