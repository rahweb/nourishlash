<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="img pro-det">
            <div class="overlay" style="height: 40.5%;">
            </div>
            <img style="width:100%" src="images/slider/Layer copy.png" alt="">
        </div>
        <div class="container">
            <div class="title-e text-left">
                <br>
                <ul>
                    <li class="home"><a href="#">HOME</a></li>
                    <li><a>CONTACT US</a></li>
                </ul>
                <br>
            </div>
        </div>
        <div class="contact">
            <div class="container">
                <div class="row">
                <div class="container animated fadeIn">
                    <div class="row">
                        <h1 class="header-title">CONTACT US</h1>
                        <hr>
                        <div class="col-sm-12" id="parent">
                            <!-- <div class="col-sm-6 float-right">
                                <iframe width="100%" height="320px;" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJaY32Qm3KWTkRuOnKfoIVZws&key=AIzaSyAf64FepFyUGZd3WFWhZzisswVx2K37RFY" allowfullscreen></iframe>
                            </div> -->
                            <div class="">
                                <form action="" class="contact-form" method="post">
                                    <input type="hidden" name="_token" value="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" required="" autofocus="">
                                    </div>
                                    <div class="form-group form_left">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="mobile" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="10" placeholder="Mobile No." required="">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control textarea-contact" rows="5" id="content" name="content" placeholder="Type Your Message ..." required=""></textarea>
                                        <br>
                                        <button class="btn btn-default btn-send" type="submit">
                                            <span class="glyphicon glyphicon-send"></span> Send
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="">
                        <br>
                        <div class="row text-center">
                            <div class="col-sm-3 col-xs-6 first-box">
                                <h1><img src="https://besteyelashgrowthserumsreviews.com/assets/site/images/icon/telephone.png" alt=""></h1>
                                <h3>Phone</h3>
                                <p>+880-1700-987654</p>
                            </div>
                            <div class="col-sm-3 col-xs-6 second-box">
                                <h1><img src="https://besteyelashgrowthserumsreviews.com/assets/site/images/icon/placeholder.png" alt=""></h1>
                                <h3>Location</h3>
                                <p>1036 Gulshan Road</p>
                            </div>
                            <div class="col-sm-3 col-xs-6 third-box">
                                <h1><img src="https://besteyelashgrowthserumsreviews.com/assets/site/images/icon/close-envelope.png" alt=""></h1>
                                <h3>E-mail</h3>
                                <p>info@yourdomain.com</p>
                            </div>
                            <div class="col-sm-3 col-xs-6 fourth-box">
                                <h1><img src="https://besteyelashgrowthserumsreviews.com/assets/site/images/icon/global.png" alt=""></h1>
                                <h3>Web</h3>
                                <p>www.yourdomain.com</p>
                            </div>
                        </div>
                    </div> -->
                </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
    </body>
    <?php include("blocks/script.php");?>
</html>