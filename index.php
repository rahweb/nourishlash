<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <?php include("blocks/slider.php");?>
        <?php include("blocks/introduction.php");?>
        <?php include("blocks/about.php");?>
        <?php include("blocks/research.php");?>
        <?php include("blocks/Submit.php");?>
        <?php include("blocks/footer.php");?>
    </body>
    <?php include("blocks/script.php");?>
</html>