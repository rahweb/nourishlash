<div class="submit">
    <div class="container">
        <!-- <div class="text">
            <h1 style="margin:0%">
                SUBMIT YOUR REVIEW
            </h1>
            <p>
                PLEASE CLICK HERE THAT SUBMIT A REVIEW FOR BRAND USE
            </p>
        </div> -->
        <div class="row img">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                <img src="images/submit-11.png" alt="">
            </button>
            <div class="modal fade" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <center>  <span class="modal-title">SUBMIT A REVIEW</span></center>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-area">
                                <form method="POST" role="form" accept-charset="UTF-8" action="https://besteyelashgrowthserumsreviews.com/comment">
                                    <input type="hidden" name="_token" value="zYiGP3EjG2MfvKQ03iK9Tn2ENFEfXpjSrOmdTiBZ">
                                    <br style="clear:both">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" required="">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email" required="">
                                    </div>
                                    <div class="wrap-select">
                                        <span style="float:left;color:gray;margin-bottom: 3px">Product Name</span>
                                        <select name="product_id" id="product_id" class="form-control" v-model="product_id">
                                            <option value="">Product Name</option>
                                            <option value="1">NourishLash</option>
                                            <option value="5">Revitalash</option>
                                            <option value="4">GrandeLash</option>
                                            <option value="3">VegaLash</option>
                                            <option value="2">Neulash</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" type="textarea" name="content" id="content" placeholder="Message" rows="7"></textarea>
                                    </div>
                                </form>
                                <div class="container">
                                    <div class="row">
                                        <div class="rating">
                                        <input type="radio" id="star10" name="rating" value="10" /><label for="star10" title="Rocks!">5 stars</label>
                                        <input type="radio" id="star9" name="rating" value="9" /><label for="star9" title="Rocks!">4 stars</label>
                                        <input type="radio" id="star8" name="rating" value="8" /><label for="star8" title="Pretty good">3 stars</label>
                                        <input type="radio" id="star7" name="rating" value="7" /><label for="star7" title="Pretty good">2 stars</label>
                                        <input type="radio" id="star6" name="rating" value="6" /><label for="star6" title="Meh">1 star</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit
                                        Form
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>