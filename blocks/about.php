<div class="about">

    <div class="row">
        <div class="title">
            <img src="images/slider/br-3.png" alt="">
            <h2>TODAYS 7  <span>BEST EYELASH SERUMS</span>  WITH REVIEWS – 2019</h2>
        </div>
    </div>
    <br>
</div>
<div class="team">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <p>
                    For this review, I and my team have read dozens of beauty blogs, watch YouTube videos, and also tried some lash products to provide you useful information and help you in your buying decision.
                    <br>
                    At the end of the day, it doesn’t matter how expensive the product is or how big the brand name is, the most important thing is if it provides results and offers the best value for money.
                    <br>
                    Of course, we cannot guarantee that these products will work for you as each person has varying growth length and skin type. But based on our review, those lash serums that we mentioned below offer amazing results.
                </p>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <img src="images/slider/about.png" alt="">
            </div>
        </div>
    </div>
</div>